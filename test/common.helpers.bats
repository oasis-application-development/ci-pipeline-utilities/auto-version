#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/common"

setup() {
  export LOG_LEVEL="info"
}

@test "$(basename ${BATS_TEST_FILENAME}) check_required_environment takes a required environment variable, and raises an error if it is missing" {
  source ${profile_script}
  local required_environment="FOO"
  assert_empty "${FOO}"

  function error() { echo "error ${*} called" >&2; }
 
  run check_required_environment "${required_environment}"
  assert_failure
  assert_output --partial "error missing ENVIRONMENT ${required_environment}! called"
}

@test "$(basename ${BATS_TEST_FILENAME}) check_required_environment takes a list of required environment variables, and raises an error if one is missing" {
  source ${profile_script}

  local required_environment="FOO BAR"
  local missing_environment_variable="BAR"
  local existing_environment_variable="FOO"
  export FOO="foo"
  assert_empty "${BAR}"

  function error() { echo "error ${*} called" >&2; }

  run check_required_environment "${required_environment}"
  assert_failure
  assert_output --partial "error missing ENVIRONMENT ${missing_environment_variable}! called"
  refute_output --partial "error missing ENVIRONMENT ${existing_environment_variable}! called"
  unset FOO
}

@test "$(basename ${BATS_TEST_FILENAME}) check_required_environment takes a list of required environment variables, and returns success if all are present" {
  local required_environment="FOO BAR"
  export FOO="foo"
  export BAR="bar"
  source ${profile_script}
  run check_required_environment "${required_environment}"
  assert_success
  unset FOO
  unset BAR
}

@test "$(basename ${BATS_TEST_FILENAME}) dry_run returns false if DRY_RUN environment variable is not set" {
  assert_empty "${DRY_RUN}"
  source ${profile_script}
  run dry_run
  assert_failure
}

@test "$(basename ${BATS_TEST_FILENAME}) dry_run prints an info message and returns successful if DRY_RUN environment variable is set" {
  source ${profile_script}
  export DRY_RUN=1

  function info() { echo "info ${*} called" >&2; }

  run dry_run
  assert_output "info skipping for dry run called"
  unset DRY_RUN
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_version generates the first version if no tags are found in the repo" {
  source ${profile_script}
  local expected_version='v0.0.0'

  function get() { echo "get ${*}" >&2; echo '[]'; }

  run get_latest_version
  assert_success
  assert_output -p "get /repository/tags"
  assert_output -p "${expected_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_version generates the first \$at_major_version version if no tags are found in the repo matching \$at_major_version" {
  source ${profile_script}
  local expected_at_major_version='v1'
  local expected_version='v1.0.0'

  function get() { echo "get ${*}" >&2; echo '[{"name":"v0.0.1"}]'; }

  run get_latest_version "${expected_at_major_version}"
  assert_success
  assert_output -p "get /repository/tags"
  assert_output -p "${expected_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_version generates the first version tag if a version tag does not already exist in the repo" {
  source ${profile_script}
  local expected_version='v0.0.0'

  function get() { echo "get ${*}" >&2; echo '[{"name":"sometag"}]'; }

  run get_latest_version
  assert_success
  assert_output -p "get /repository/tags"
  assert_output -p "${expected_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_version returns the latest version tag if one exists in the repo" {
  source ${profile_script}
  local expected_version='v0.0.1'

  function get() { echo "get ${*}" >&2; echo '[{"name":"v0.0.1"}]'; }

  run get_latest_version
  assert_success
  assert_output -p "get /repository/tags"
  assert_output -p "${expected_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_version returns the latest $at_major_version version tag if one exists in the repo" {
  source ${profile_script}
  local expected_at_major_version="v1"
  local expected_version='v1.0.1'

  function get() { echo "get ${*}" >&2; echo '[{"name":"v0.0.1"},{"name":"v1.0.1"}]'; }

  run get_latest_version "${expected_at_major_version}"
  assert_success
  assert_output -p "get /repository/tags"
  assert_output -p "${expected_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_version does not return release candidate tags in the repo" {
  source ${profile_script}
  local expected_version='v0.0.1'
  local release_candidate="v0.0.1-rc.0"

  function get() { echo "get ${*}" >&2; echo '[{"name":"v0.0.1-rc.0"},{"name":"v0.0.1"}]'; }

  run get_latest_version
  assert_success
  assert_output -p "get /repository/tags"
  assert_output -p "${expected_version}"
  refute_output -p "${release_candidate}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_version does not return $at_major_version release candidate tags in the repo" {
  source ${profile_script}
  local expected_at_major_version="v1"
  local expected_version='v1.0.1'
  local release_candidate="v1.0.1-rc.0"

  function get() { echo "get ${*}" >&2; echo '[{"name":"v0.0.1-rc.0"},{"name":"v0.0.1"},{"name":"v1.0.1-rc.0"},{"name":"v1.0.1"}]'; }

  run get_latest_version "${expected_at_major_version}"
  assert_success
  assert_output -p "get /repository/tags"
  assert_output -p "${expected_version}"
  refute_output -p "${release_candidate}"
}

@test "$(basename ${BATS_TEST_FILENAME}) increment takes current_version and uses semver to increment it with the patch_level" {
  source ${profile_script}
  local current_version='v0.0.1'
  #note semver strips leading 'v'
  local expected_new_version='0.0.2'

  function semver() { echo "semver ${*}" >&2; echo "0.0.2"; }
  function patch_level() { echo "patch_level called" >&2; echo "patch"; }

  run increment "${current_version}"
  assert_success
  assert_output -p "patch_level called"
  assert_output -p "semver -i patch ${current_version}"
  assert_output -p "${expected_new_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) increment takes current_version and preid, and uses semver to increment it with the patch_level and preid" {
  source ${profile_script}
  local current_version='v0.0.1'
  #note semver strips leading 'v'
  local preid="rc"
  local expected_new_version="0.0.1-${preid}.0"

  function semver() { echo "semver ${*}" >&2; echo "0.0.1-rc.0"; }
  function patch_level() { echo "patch_level called" >&2; echo "prepatch"; }

  run increment "${current_version}" "${preid}"
  assert_success
  assert_output -p "patch_level called"
  assert_output -p "semver -i prepatch ${current_version} --preid ${preid}"
  assert_output -p "${expected_new_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) increment fails if patch_level fails" {
  source ${profile_script}
  local current_version='v0.0.1'
  #note semver strips leading 'v'
  local expected_new_version='0.0.2'

  function semver() { echo "semver ${*}" >&2; echo "0.0.2"; }
  function patch_level() { echo "patch_level called" >&2; echo "patch"; return 1; }

  run increment "${current_version}"
  assert_failure
  assert_output -p "patch_level called"
  refute_output -p "semver -i patch ${current_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) increment fails if semver fails" {
  source ${profile_script}
  local current_version='v0.0.1'

  function semver() { echo "semver ${*}" >&2; return 1; }
  function patch_level() { echo "patch_level called" >&2; echo "patch"; }

  run increment "${current_version}"
  assert_failure
  assert_output -p "patch_level called"
  assert_output -p "semver -i patch ${current_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_version takes a new_version and posts it as an annotated tag to gitlab" {
  source ${profile_script}
  local expected_new_version="v1.0.1"

  function post() { echo "post ${*}" >&2; }

  run tag_version "${expected_new_version}"
  assert_success
  assert_output -p "Tagging ${CI_PROJECT_NAME} at ${CI_API_V4_URL} with ${expected_new_version}"
  assert_output -p "post {\"tag_name\":\"${expected_new_version}\",\"ref\":\"${CI_COMMIT_SHA}\",\"message\":\"${expected_new_version} generated by semver bot\"} /repository/tags"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_version prints BOT_NAME in tag message if present" {
  source ${profile_script}
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  export CI_PROJECT_NAME="CI_PROJECT_NAME"
  export CI_API_V4_URL="CI_API_V4_URL"
  export BOT_NAME="testbot"
  local expected_new_version="v1.0.1"
  local expected_data='{"tag_name":"'${expected_new_version}'","ref":"'${CI_COMMIT_SHA}'","message":"'"${expected_new_version} generated by ${BOT_NAME} bot"'"}'

  function post() { echo "post ${*}" >&2; }

  run tag_version "${expected_new_version}"
  assert_success
  assert_output -p "Tagging ${CI_PROJECT_NAME} at ${CI_API_V4_URL} with ${expected_new_version}"
  assert_output -p "post ${expected_data} /repository/tags"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_version prints supplied message in tag message if present" {
  source ${profile_script}
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  export CI_PROJECT_NAME="CI_PROJECT_NAME"
  export CI_API_V4_URL="CI_API_V4_URL"
  local expected_new_version="v1.0.1"
  local expected_message='test-message\nwith newlines'
  local expected_data='{"tag_name":"'${expected_new_version}'","ref":"'${CI_COMMIT_SHA}'","message":"'"${expected_message}"'"}'
  function post() { echo "post ${*}" >&2; }

  run tag_version "${expected_new_version}" "${expected_message}"
  assert_success
  assert_output -p "Tagging ${CI_PROJECT_NAME} at ${CI_API_V4_URL} with ${expected_new_version}"
  assert_output -p "post ${expected_data} /repository/tags"
}
