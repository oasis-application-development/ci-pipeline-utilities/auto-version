#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/auto_version"

setup() {
  export CI_COMMIT_MESSAGE="CI_COMMIT_MESSAGE"
  export CI_PROJECT_NAME="CI_PROJECT_NAME"
  export BOT_KEY="BOT_KEY"
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  export CI_API_V4_URL="CI_API_V4_URL"
  export CI_PROJECT_ID="CI_PROJECT_ID"
  export LOG_LEVEL="info"
}

@test "$(basename ${BATS_TEST_FILENAME}) patch_level returns patch if SEMVER_VERSION_LEVEL is not present" {
  local expected_patch_level="patch"
  source ${profile_script}
  run patch_level
  assert_success
  assert_output "${expected_patch_level}"
}

@test "$(basename ${BATS_TEST_FILENAME}) patch_level returns value of SEMVER_VERSION_LEVEL if present in CI_COMMIT_MESSAGE" {
  local expected_patch_level="major"
  CI_COMMIT_MESSAGE="SEMVER_VERSION_LEVEL:${expected_patch_level}"
  source ${profile_script}
  run patch_level
  assert_success
  assert_output "${expected_patch_level}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main checks required environment, gets the latest version, increments it, and creates a new tag prepended with 'v'" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'
  local expected_required_environment="CI_COMMIT_MESSAGE CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function check_required_environment() { echo "check_required_environment ${*} called"; }
  export -f check_required_environment
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version

  run run_main
  assert_success
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "get_latest_version"
  assert_output -p "increment ${expected_current_version}"
  assert_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if check_required_environment fails" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'
  local expected_required_environment="CI_COMMIT_MESSAGE CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function check_required_environment() { echo "check_required_environment ${*} called"; return 1; }
  export -f check_required_environment
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version

  run run_main
  assert_failure
  assert_output -p "check_required_environment ${expected_required_environment} called"
  refute_output -p "get_latest_version"
  refute_output -p "increment ${expected_current_version}"
  refute_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if increment fails" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'

  function check_required_environment() { echo "check_required_environment ${*} called"; }
  export -f check_required_environment
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; return 1; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version

  run run_main
  assert_failure
  assert_output -p "problem incrementing"
  assert_output -p "check_required_environment"
  assert_output -p "get_latest_version"
  assert_output -p "increment ${expected_current_version}"
  refute_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if tag_version fails" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'

  function check_required_environment() { echo "check_required_environment ${*}"; }
  export -f check_required_environment
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; return 1; }
  export -f tag_version

  run run_main
  assert_failure
  assert_output -p "check_required_environment"
  assert_output -p "get_latest_version"
  assert_output -p "increment ${expected_current_version}"
  assert_output -p "tag_version ${expected_tag_version}"
}
