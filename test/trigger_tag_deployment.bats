#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/trigger_tag_deployment"

@test "$(basename ${BATS_TEST_FILENAME})#check_required_trigger_environment fails if check_required_environment fails" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; return 1; }

  run check_required_trigger_environment
  assert_failure
  assert_output -p 'check_required_environment TRIGGER_DEPLOY_TARGET CI_JOB_TOKEN CI_COMMIT_TAG CI_API_V4_URL CI_PROJECT_ID'
}

@test "$(basename ${BATS_TEST_FILENAME})#check_required_trigger_environment succeeds if check_required_environment succeeds" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*}" >&2; }

  run check_required_trigger_environment
  assert_success
  assert_output -p 'check_required_environment TRIGGER_DEPLOY_TARGET CI_JOB_TOKEN CI_COMMIT_TAG CI_API_V4_URL CI_PROJECT_ID'
}

@test "$(basename ${BATS_TEST_FILENAME})#trigger_job fails if response json does not include a web_url" {
  source ${profile_script}

  function curl() { echo "curl ${*}" >&2; echo '{"message":"404 Not Found"}'; }
  export TRIGGER_DEPLOY_TARGET='TRIGGER_DEPLOY_TARGET'
  export CI_JOB_TOKEN='CI_JOB_TOKEN'
  export CI_COMMIT_TAG='CI_COMMIT_TAG'
  export CI_API_V4_URL='CI_API_V4_URL'
  export CI_PROJECT_ID='CI_PROJECT_ID'

  run trigger_job
  assert_failure
  assert_output -p "triggering ${TRIGGER_DEPLOY_TARGET} deployment"
  assert_output -p '{"message":"404 Not Found"}'
}

@test "$(basename ${BATS_TEST_FILENAME})#trigger_job succeeds if response succeeds" {
  source ${profile_script}

  function curl() { echo "curl ${*}" >&2; echo '{"id":"abc123","web_url":"https://foo.bar"}'; }
  export TRIGGER_DEPLOY_TARGET='TRIGGER_DEPLOY_TARGET'
  export CI_JOB_TOKEN='CI_JOB_TOKEN'
  export CI_COMMIT_TAG='CI_COMMIT_TAG'
  export CI_API_V4_URL='CI_API_V4_URL'
  export CI_PROJECT_ID='CI_PROJECT_ID'

  run trigger_job
  assert_success
  assert_output -p "triggering ${TRIGGER_DEPLOY_TARGET} deployment"
  assert_output -p '{"id":"abc123","web_url":"https://foo.bar"}'
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if required environment is not present" {
  source ${profile_script}

  function check_required_trigger_environment() { echo "check_required_trigger_environment called" >&2; return 1; }
  function trigger_job() { echo "trigger_job called" >&2; }

  run run_main
  assert_failure
  assert_output -p "check_required_trigger_environment called"
  refute_output -p "trigger_job_called"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main fails if trigger_job fails" {
  source ${profile_script}

  function check_required_trigger_environment() { echo "check_required_trigger_environment called" >&2; }
  function trigger_job() { echo "trigger_job called" >&2; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_required_trigger_environment called"
  assert_output -p "trigger_job called"
}

@test "$(basename ${BATS_TEST_FILENAME})#run_main succeds if required environment is present and trigger_job succeeds" {
  source ${profile_script}

  function check_required_trigger_environment() { echo "check_required_trigger_environment called" >&2; }
  function trigger_job() { echo "trigger_job called" >&2; }

  run run_main
  assert_success
  assert_output -p "check_required_trigger_environment called"
  assert_output -p "trigger_job called"
}

@test "$(basename ${BATS_TEST_FILENAME})#trigger_info" {
  source ${profile_script}

  export DEPLOY_TARGET='DEPLOY_TARGET'
  export CI_COMMIT_REF_NAME='CI_COMMIT_REF_NAME'

  run trigger_info
  assert_output "triggering ${DEPLOY_TARGET} deployment on ${CI_COMMIT_REF_NAME}"
}