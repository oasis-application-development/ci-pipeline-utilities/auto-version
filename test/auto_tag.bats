#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/auto_tag"

setup() {
  export CI_COMMIT_MESSAGE="CI_COMMIT_MESSAGE"
  export CI_PROJECT_NAME="CI_PROJECT_NAME"
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  export CI_API_V4_URL="CI_API_V4_URL"
  export CI_PROJECT_ID="CI_PROJECT_ID"
  export LOG_LEVEL="info"
}

@test "$(basename ${BATS_TEST_FILENAME}) private_token returns value of BOT_KEY Variable" {
  source ${profile_script}
  export BOT_KEY="BOT_KEY"
  run private_token
  assert_output "${BOT_KEY}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_line returns options from [auto_tag options] block if present in CI_COMMIT_MESSAGE" {
  source ${profile_script}

  expected_tag_line='prepatch preid release! @v4'
  CI_COMMIT_MESSAGE="This fixes a bug [auto_tag ${expected_tag_line}]"
  run tag_line
  assert_success
  assert_output "${expected_tag_line}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_line returns value of AUTO_TAG Variable if present" {
  source ${profile_script}

  expected_tag_line='patch @v4'
  export AUTO_TAG="${expected_tag_line}"
  run tag_line
  assert_success
  assert_output "${expected_tag_line}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_line returns value of AUTO_TAG Variable if both AUTO_TAG Variable and CI_COMMIT_MESSAGE block are present" {
  source ${profile_script}

  expected_tag_line='patch @v4'
  unexpected_tag_line='major'
  CI_COMMIT_MESSAGE="This fixes a bug [auto_tag ${unexpected_tag_line}]"
  export AUTO_TAG="${expected_tag_line}"
  run tag_line
  assert_success
  assert_output "${expected_tag_line}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag_line returns empty string if AUTO_TAG Variable or line are not present" {
  source ${profile_script}
  unset AUTO_TAG
  expected_tag_line=""
  run tag_line
  assert_output "${expected_tag_line}"
}

@test "$(basename ${BATS_TEST_FILENAME}) patch_level returns value of tag_line increment" {
  source ${profile_script}

  local expected_patch_level="major"
  function tag_line() { echo 'major release! @v4'; }
  export -f tag_line

  run patch_level
  assert_output "${expected_patch_level}"
}

@test "$(basename ${BATS_TEST_FILENAME}) patch_level returns empty increment if increment found in tag_line is not supported" {
  source ${profile_script}

  local expected_patch_level=""
  function tag_line() { echo 'notsupported'; }
  export -f tag_line

  run patch_level
  assert_output "${expected_patch_level}"
}

@test "$(basename ${BATS_TEST_FILENAME}) release returns true if release! is present in the tag_line, and is_release_candidate is false" {
  source ${profile_script}

  function tag_line() { echo 'tag_line called' >&2; echo 'patch release! @v4'; }
  export -f tag_line
  function is_release_candidate() { echo 'is_release_candidate called' >&2; return 1; }
  export -f is_release_candidate

  run release
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p 'is_release_candidate called'
}

@test "$(basename ${BATS_TEST_FILENAME}) release returns false if is_release_candidate is false and release! is not present in the tag_line" {
  source ${profile_script}

  function tag_line() { echo 'tag_line called' >&2; echo 'preminor @v4'; }
  export -f tag_line
  function is_release_candidate() { echo 'is_release_candidate called' >&2; return 1; }
  export -f is_release_candidate

  run release
  assert_failure
  assert_output -p 'is_release_candidate called'
  assert_output -p 'tag_line called'
}

@test "$(basename ${BATS_TEST_FILENAME}) release returns false if is_release_candidate is true" {
  source ${profile_script}

  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch release!'; }
  export -f tag_line
  function is_release_candidate() { return; }
  export -f is_release_candidate

  run release
  assert_failure
  refute_output "tag_line called"
}

@test "$(basename ${BATS_TEST_FILENAME}) is_release_candidate returns true if patch_level starts with pre" {
  source ${profile_script}

  function patch_level() { echo 'patch_level called' >&2; echo 'prepatch'; }
  export -f patch_level

  run is_release_candidate
  assert_success
  assert_output 'patch_level called'
}

@test "$(basename ${BATS_TEST_FILENAME}) is_release_candidate returns false if patch_level does not start with pre" {
  source ${profile_script}

  function patch_level() { echo 'patch_level called' >&2; echo 'patch'; }
  export -f patch_level

  run is_release_candidate
  assert_failure
  assert_output 'patch_level called'
}

@test "$(basename ${BATS_TEST_FILENAME}) preid returns empty if is_release_candidate is false" {
  source ${profile_script}

  expected_preid=""
  function tag_line() { echo 'tag_line called' >&2; echo 'major'; }
  export -f tag_line
  function is_release_candidate() { echo 'is_release_candidate called'; return 1; }
  export -f is_release_candidate

  run preid
  assert_success
  assert_output -p 'is_release_candidate called'
  refute_output -p 'tag_line called'
  assert_output -p "${expected_preid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) preid returns non-zero if is_release_candidate is true but preid does not exist in tag_line" {
  source ${profile_script}

  expected_preid=""
  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch'; }
  export -f tag_line
  function is_release_candidate() { echo 'is_release_candidate called'; }
  export -f is_release_candidate

  run preid
  assert_failure
  assert_output -p 'is_release_candidate called'
  assert_output -p 'tag_line called'
  assert_output -p "${expected_preid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) preid returns value of preid in tag_line if is_release_candidate is true and preid exists" {
  source ${profile_script}

  expected_preid="rc"
  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch rc @v4'; }
  export -f tag_line
  function is_release_candidate() { echo 'is_release_candidate called'; }
  export -f is_release_candidate

  run preid
  assert_success
  assert_output -p 'is_release_candidate called'
  assert_output -p 'tag_line called'
  assert_output -p "${expected_preid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) preid returns nonzero if preid in tag_line is release!" {
  source ${profile_script}

  expected_preid=""
  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch release! @v4'; }
  export -f tag_line
  function is_release_candidate() { echo 'is_release_candidate called'; }
  export -f is_release_candidate

  run preid
  assert_failure
  assert_output -p 'is_release_candidate called'
  assert_output -p 'tag_line called'
  assert_output -p "${expected_preid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) preid returns nonzero if preid in tag_line matches @vM" {
  source ${profile_script}

  expected_preid=""
  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch @v4'; }
  export -f tag_line
  function is_release_candidate() { echo 'is_release_candidate called'; }
  export -f is_release_candidate

  run preid
  assert_failure
  assert_output -p 'is_release_candidate called'
  assert_output -p 'tag_line called'
  assert_output -p "${expected_preid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) at_major_version returns vM if @vM is present and patch_level is not major" {
  source ${profile_script}

  local expected_at_major_version='v4'
  function tag_line() { echo 'tag_line called' >&2; echo 'minor @v4'; }
  export -f tag_line
  function patch_level { echo 'patch_level called' >&2; echo 'minor'; }
  export -f patch_level

  run at_major_version
  assert_success
  assert_output -p 'tag_line called';
  assert_output -p 'patch_level called';
  refute_output -p '@v4'
  assert_output -p "${expected_at_major_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) at_major_version returns empty if @vM is not present" {
  source ${profile_script}

  local expected_at_major_version=''
  function tag_line() { echo 'tag_line called' >&2; echo 'minor'; }
  export -f tag_line
  function patch_level { echo 'patch_level called' >&2; echo 'minor'; }
  export -f patch_level

  run at_major_version
  assert_success
  assert_output -p 'tag_line called';
  refute_output -p 'patch_level called';
  assert_output -p "${expected_at_major_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) at_major_version returns non-zero if @vM is present and patch_level is major" {
  source ${profile_script}

  local expected_at_major_version=''
  function tag_line() { echo 'tag_line called' >&2; echo 'major @v4'; }
  export -f tag_line
  function patch_level { echo 'patch_level called' >&2; echo 'major'; }
  export -f patch_level

  run at_major_version
  assert_failure
  assert_output -p 'tag_line called';
  assert_output -p 'patch_level called';
  assert_output -p "${expected_at_major_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_release_candidate_for returns latest release_candidate tag for tag matching preid if it exists" {
  source ${profile_script}

  local preid="rc"
  local expected_candidate_release="v0.0.2-${preid}.0"
  local expected_private_token='PT'
  function private_token() { echo 'private_token called' >&2; echo 'PT'; }
  export -f private_token
  function curl() { echo "curl ${*}" >&2; echo '[{"name":"v0.0.1"},{"name":"v0.0.2-rc.0"}]'; }
  export -f curl

  run get_latest_release_candidate_for "${preid}"
  assert_output -p 'private_token called'
  assert_output -p "curl --header Content-Type: application/json --header Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags"
  assert_output -p "${expected_candidate_release}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_release_candidate_for returns latest release_candidate for @vM tag if provided, and matching preid if both exist" {
  source ${profile_script}

  local at_major_version='v4'
  local preid="rc"
  local expected_candidate_release="v4.0.2-${preid}.0"
  local expected_private_token='PT'
  function private_token() { echo 'private_token called' >&2; echo 'PT'; }
  export -f private_token
  function curl() { echo "curl ${*}" >&2; echo '[{"name":"v4.0.1"},{"name":"v3.4.12-rc.10"},{"name":"v4.0.2-rc.0"}]'; }
  export -f curl

  run get_latest_release_candidate_for "${preid}" "${at_major_version}"
  assert_output -p 'private_token called'
  assert_output -p "curl --header Content-Type: application/json --header Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags"
  assert_output -p "${expected_candidate_release}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_release_candidate_for returns empty string if no release candidate tag for the preid exists" {
  source ${profile_script}

  local expected_candidate_release=""
  local preid="rc"
  local expected_private_token='PT'
  function private_token() { echo 'private_token called' >&2; echo 'PT'; }
  export -f private_token
  function curl() { echo "curl ${*}" >&2; echo '[{"name":"v0.0.1"},{"name":"v0.0.2-otherpre.0"}]'; }
  export -f curl

  run get_latest_release_candidate_for "${preid}"
  assert_output -p 'private_token called'
  assert_output -p "curl --header Content-Type: application/json --header Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags"
  assert_output -p "${expected_candidate_release}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get_latest_release_candidate_for returns empty string if no release candidate @vM tag for the preid and @vM exists" {
  source ${profile_script}

  local at_major_version='v2'
  local expected_candidate_release=""
  local preid="rc"
  local expected_private_token='PT'
  function private_token() { echo 'private_token called' >&2; echo 'PT'; }
  export -f private_token
  function curl() { echo "curl ${*}" >&2; echo '[{"name":"v2.0.1"},{"name":"v2.0.2-otherpre.0"}]'; }
  export -f curl

  run get_latest_release_candidate_for "${preid}" "${at_major_version}"
  assert_output -p 'private_token called'
  assert_output -p "curl --header Content-Type: application/json --header Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/repository/tags"
  assert_output -p "${expected_candidate_release}"
}

@test "$(basename ${BATS_TEST_FILENAME}) release_description returns empty string if release is false" {
  source ${profile_script}

  export CI_COMMIT_MESSAGE="This fixes a bug [auto_tag major]"
  local expected_release_description=""
  function release() { echo 'release called' >&2; return 1; }
  export -f release

  run release_description
  assert_output -p 'release called'
  assert_output -p "${expected_release_description}"
}

@test "$(basename ${BATS_TEST_FILENAME}) release_description returns line in commit message that contains auto_tag, without the auto_tag block, if release is true" {
  source ${profile_script}

  unexpected_part="This is not expected"
  export CI_COMMIT_MESSAGE="This fixes a bug [auto_tag major release! @v4]
  
  ${unexpected_part}
  "
  local expected_release_description="This fixes a bug"
  function release() { echo 'release called' >&2; }
  export -f release

  run release_description
  assert_output -p 'release called'
  assert_output -p "${expected_release_description}"
  refute_output -p "${unexpected_part}"
}

@test "$(basename ${BATS_TEST_FILENAME}) release_version takes tag and does not create a release if release returns false" {
  source ${profile_script}
  local expected_tag='v0.1.2'
  local expected_private_token='PT'

  function release() { echo 'release called' >&2; return 1; }
  export -f release
  function release_description() { echo 'release_description called' >&2; echo ""; }
  export -f release_description
  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  export -f private_token
  function curl() { echo "curl called" >&2; }
  export -f curl

  run release_version "${expected_tag}"
  assert_output -p 'release called'
  refute_output -p 'release_description called'
  refute_output -p "curl called"
}

@test "$(basename ${BATS_TEST_FILENAME}) release_version takes tag and creates a release with release_description if release returns true" {
  source ${profile_script}
  local expected_tag='v0.1.2'
  local expected_new_release='new release'
  local expected_private_token='PT'

  function release() { echo 'release called' >&2; }
  export -f release
  function release_description() { echo 'release_description called' >&2; echo "new release"; }
  export -f release_description
  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  export -f private_token
  function curl() { echo "curl ${*}" >&2; }
  export -f curl

  run release_version "${expected_tag}"
  assert_output -p 'release called'
  assert_output -p 'release_description called'
  assert_output -p "Releasing ${expected_tag} as ${expected_new_release}"
  assert_output -p "curl -X POST --header Content-Type: application/json --header Accept: application/json -H Private-Token: ${expected_private_token} -d {\"name\":\"${expected_tag}\",\"tag_name\":\"${expected_tag}\",\"description\":\"${expected_new_release}\"} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main does not run if tag_line returns empty" {
  source ${profile_script}

  function tag_line() { echo 'tag_line called' >&2; echo ""; }
  export -f tag_line
  function check_required_environment() { echo 'check_required_environment called' >&2; }
  export -f check_required_environment
  function patch_level() { echo 'patch_level called' >&2; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { 'get_latest_release_candidate_for called' >&2; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; }
  export -f get_latest_version
  function increment() { echo 'increment called' >&2; }
  export -f increment
  function tag_version() { echo 'tag_version called' >&2; }
  export -f tag_version
  function release_version() { echo 'release_version called' >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p 'no auto_tag block detected, skipping'
  refute_output -p 'check_required_environment called'
  refute_output -p 'patch_level called'
  refute_output -p 'preid called'
  refute_output -p 'at_major_version called'
  refute_output -p 'is_release_candidate called'
  refute_output -p 'get_latest_release_candidate_for called'
  refute_output -p 'get_latest_version called'
  refute_output -p 'increment called'
  refute_output -p 'tag_version called'
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if check_required_environment fails" {
  source ${profile_script}

  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'patch'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; return 1; }
  export -f check_required_environment
  function patch_level() { echo 'patch_level called' >&2; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { 'get_latest_release_candidate_for called' >&2; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; }
  function increment() { echo 'increment called' >&2; }
  export -f increment
  function tag_version() { echo 'tag_version called' >&2; }
  export -f tag_version
  function release_version() { echo 'release_version called' >&2; }
  export -f release_version

  run run_main
  assert_failure
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  refute_output -p 'patch_level called'
  refute_output -p 'preid called'
  refute_output -p 'at_major_version called'
  refute_output -p 'is_release_candidate called'
  refute_output -p 'get_latest_release_candidate_for called'
  refute_output -p 'get_latest_version called'
  refute_output -p 'increment called'
  refute_output -p 'tag_version called'
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if patch_level returns empty" {
  source ${profile_script}

  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'unsupported_patch_level'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo 'patch_level called' >&2; echo ""; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { 'get_latest_release_candidate_for called' >&2; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; }
  export -f get_latest_version
  function increment() { echo 'increment called' >&2; }
  export -f increment
  function tag_version() { echo 'tag_version called' >&2; }
  export -f tag_version
  function release_version() { echo 'release_version called' >&2; }
  export -f release_version

  run run_main
  assert_failure
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  refute_output -p 'preid called'
  refute_output -p 'at_major_version called'
  refute_output -p 'is_release_candidate called'
  refute_output -p 'get_latest_release_candidate_for called'
  refute_output -p 'get_latest_version called'
  refute_output -p 'increment called'
  refute_output -p 'tag_version called'
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if preid fails" {
  source ${profile_script}

  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo 'patch_level called' >&2; echo 'prepatch'; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; return 1; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { 'get_latest_release_candidate_for called' >&2; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; }
  export -f get_latest_version
  function increment() { echo 'increment called' >&2; }
  export -f increment
  function tag_version() { echo 'tag_version called' >&2; }
  export -f tag_version
  function release_version() { echo 'release_version called' >&2; }
  export -f release_version
  
  run run_main
  assert_failure
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  refute_output -p 'at_major_version called'
  refute_output -p 'is_release_candidate called'
  refute_output -p 'get_latest_release_candidate_for called'
  refute_output -p 'get_latest_version called'
  refute_output -p 'increment called'
  refute_output -p 'tag_version called'
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if at_major_version fails" {
  source ${profile_script}

  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'major @v2'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo 'major'; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo ""; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; return 1; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { 'get_latest_release_candidate_for called' >&2; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; }
  export -f get_latest_version
  function increment() { echo 'increment called' >&2; }
  export -f increment
  function tag_version() { echo 'tag_version called' >&2; }
  export -f tag_version
  function release_version() { echo 'release_version called' >&2; }
  export -f release_version

  run run_main
  assert_failure
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  refute_output -p 'is_release_candidate called'
  refute_output -p 'get_latest_release_candidate_for called'
  refute_output -p 'get_latest_version called'
  refute_output -p 'increment called'
  refute_output -p 'tag_version called'
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if increment fails" {
  source ${profile_script}

  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"
  local expected_current_version='v0.0.1'

  function tag_line() { echo 'tag_line called' >&2; echo 'unsupported_patch_level'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo 'patch_level called' >&2; echo 'patch'; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo ""; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; return 1; }
  export -f is_release_candidate
  function get_latest_version() { echo 'get_latest_version called' >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; return 1; }
  export -f increment
  function tag_version() { echo 'tag_version called' >&2; }
  export -f tag_version
  function release_version() { echo 'release_version called' >&2; }
  export -f release_version

  run run_main
  assert_failure
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p 'is_release_candidate called'
  refute_output -p 'get_latest_release_candidate_for called'
  assert_output -p 'get_latest_version called'
  assert_output -p "increment ${expected_current_version}"
  assert_output -p "problem incrementing"
  refute_output -p 'tag_version called'
}

@test "$(basename ${BATS_TEST_FILENAME}) when not a release_candidate, run_main checks required environment, patch_level, gets the latest version, increments it, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'patch'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo 'patch_level called' >&2; echo 'patch'; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo ""; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; return 1; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo 'get_latest_release_candidate_for called' >&2; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p 'is_release_candidate called'
  refute_output -p 'get_latest_release_candidate_for called'
  assert_output -p 'get_latest_version called'
  assert_output -p "increment ${expected_current_version}"
  assert_output -p "tag_version ${expected_tag_version}"
  assert_output -p "release_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) when not a release_candidate, run_main checks required environment, patch_level, gets the latest version for @vM, increments it, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_at_major_version='v1'
  local expected_current_version='v1.0.1'
  local expected_tag_version='v1.0.2'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'patch @v1'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "patch"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo ""; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; echo 'v1'; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; return 1; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo 'get_latest_release_candidate_for called' >&2; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo "get_latest_version ${*}" >&2; echo "v1.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "1.0.2"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p 'is_release_candidate called'
  refute_output -p "get_latest_release_candidate_for called"
  assert_output -p "get_latest_version ${expected_at_major_version}"
  assert_output -p "increment ${expected_current_version}"
  assert_output -p "tag_version ${expected_tag_version}"
  assert_output -p "release_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) when a release_candidate that is not a prerelease, run_main checks required environment, patch_level, gets the latest version, increments it with preid, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_preid='rc'
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2-rc.0'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch rc'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "prepatch"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo "rc"; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo "get_latest_release_candidate_for ${*}" >&2; echo ""; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "0.0.2-rc.0"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p "get_latest_release_candidate_for ${expected_preid}"
  assert_output -p 'get_latest_version called'
  assert_output -p "increment ${expected_current_version} ${expected_preid}"
  assert_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) when a release_candidate that is not a prerelease, run_main checks required environment, patch_level, gets the latest @vM version, increments it with preid, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_at_major_version='v1'
  local expected_preid='rc'
  local expected_current_version='v1.0.1'
  local expected_tag_version='v1.0.2-rc.0'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch rc @v1'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "prepatch"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo "rc"; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; echo 'v1'; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo "get_latest_release_candidate_for ${*}" >&2; echo ""; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo "get_latest_version ${*}" >&2; echo "v1.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "1.0.2-rc.0"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p "get_latest_release_candidate_for ${expected_preid} ${expected_at_major_version}"
  assert_output -p "get_latest_version ${expected_at_major_version}"
  assert_output -p "increment ${expected_current_version} ${expected_preid}"
  assert_output -p "tag_version ${expected_tag_version}"
  assert_output -p "release_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) when a release_candidate that is not a prerelease, run_main checks required environment, patch_level, gets the latest release_candidate for the preid if present, increments it with preid, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_preid='rc'
  local expected_current_version='v0.0.2-rc.0'
  local expected_tag_version='v0.0.2-rc.1'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch rc'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "prepatch"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo "rc"; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo "get_latest_release_candidate_for ${*}" >&2; echo "v0.0.2-rc.0"; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "0.0.2-rc.1"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p "get_latest_release_candidate_for ${expected_preid}"
  refute_output -p 'get_latest_version called'
  assert_output -p "increment ${expected_current_version} ${expected_preid}"
  assert_output -p "tag_version ${expected_tag_version}"
  assert_output -p "release_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) when a release_candidate that is not a prerelease, run_main checks required environment, patch_level, gets the latest @vM release_candidate for the preid if present, increments it with preid, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_at_major_version='v1'
  local expected_preid='rc'
  local expected_current_version='v1.0.2-rc.0'
  local expected_tag_version='v1.0.2-rc.1'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prepatch rc @v1'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "prepatch"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo "rc"; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; echo 'v1'; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo "get_latest_release_candidate_for ${*}" >&2; echo "v1.0.2-rc.0"; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo "get_latest_version ${*}" >&2; echo "v1.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "1.0.2-rc.1"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p "get_latest_release_candidate_for ${expected_preid} ${expected_at_major_version}"
  refute_output -p "get_latest_version ${expected_at_major_version}"
  assert_output -p "increment ${expected_current_version} ${expected_preid}"
  assert_output -p "tag_version ${expected_tag_version}"
  assert_output -p "release_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) when a release_candidate that is a prerelease, run_main fails if get_latest_release_candidate_for returns empty" {
  source ${profile_script}

  local expected_preid='rc'
  local expected_current_version='v0.0.2-rc.0'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prerelease rc'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "prerelease"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo "rc"; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo "get_latest_release_candidate_for ${*}" >&2; echo ""; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment called" >&2; echo "0.0.2-rc.1"; }
  export -f increment
  function tag_version() { echo "tag_version called"; }
  export -f tag_version
  function release_version() { echo "release_version called" >&2; }
  export -f release_version

  run run_main
  assert_failure
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p 'is_release_candidate called'
  assert_output -p "get_latest_release_candidate_for ${expected_preid}"
  refute_output -p 'get_latest_version called'
  refute_output -p "increment called"
  refute_output -p "tag_version called"
  refute_output -p "release_version called"
}

@test "$(basename ${BATS_TEST_FILENAME}) when a release_candidate that is a prerelease, run_main checks required environment, patch_level, gets the latest release_candidate for the preid, increments it with preid, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_preid='rc'
  local expected_current_version='v0.0.2-rc.0'
  local expected_tag_version='v0.0.2-rc.1'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prerelease rc'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "prerelease"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo "rc"; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo "get_latest_release_candidate_for ${*}" >&2; echo "v0.0.2-rc.0"; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; echo "v0.0.1"; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "0.0.2-rc.1"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p 'is_release_candidate called'
  assert_output -p "get_latest_release_candidate_for ${expected_preid}"
  refute_output -p 'get_latest_version called'
  assert_output -p "increment ${expected_current_version} ${expected_preid}"
  assert_output -p "tag_version ${expected_tag_version}"
  assert_output -p "release_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) when a release_candidate that is a prerelease, run_main checks required environment, patch_level, gets the latest @vM release_candidate for the preid, increments it with preid, and creates a new tag prepended with 'v'" {
  source ${profile_script}

  local expected_at_major_version='v1'
  local expected_preid='rc'
  local expected_current_version='v1.0.2-rc.0'
  local expected_tag_version='v1.0.2-rc.1'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function tag_line() { echo 'tag_line called' >&2; echo 'prerelease rc @v1'; }
  export -f tag_line
  function check_required_environment() { echo "check_required_environment ${*} called" >&2; }
  export -f check_required_environment
  function patch_level() { echo "patch_level called" >&2; echo "prerelease"; }
  export -f patch_level
  function preid() { echo 'preid called' >&2; echo "rc"; }
  export -f preid
  function at_major_version() { echo 'at_major_version called' >&2; echo 'v1'; }
  export -f at_major_version
  function is_release_candidate() { echo 'is_release_candidate called' >&2; }
  export -f is_release_candidate
  function get_latest_release_candidate_for() { echo "get_latest_release_candidate_for ${*}" >&2; echo "v1.0.2-rc.0"; }
  export -f get_latest_release_candidate_for
  function get_latest_version() { echo 'get_latest_version called' >&2; }
  export -f get_latest_version
  function increment() { echo "increment ${*}" >&2; echo "1.0.2-rc.1"; }
  export -f increment
  function tag_version() { echo "tag_version ${*}"; }
  export -f tag_version
  function release_version() { echo "release_version ${*}" >&2; }
  export -f release_version

  run run_main
  assert_success
  assert_output -p 'tag_line called'
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p 'patch_level called'
  assert_output -p 'preid called'
  assert_output -p 'at_major_version called'
  assert_output -p 'is_release_candidate called'
  assert_output -p "get_latest_release_candidate_for ${expected_preid} ${expected_at_major_version}"
  refute_output -p 'get_latest_version called'
  assert_output -p "increment ${expected_current_version} ${expected_preid}"
  assert_output -p "tag_version ${expected_tag_version}"
  assert_output -p "release_version ${expected_tag_version}"
}
