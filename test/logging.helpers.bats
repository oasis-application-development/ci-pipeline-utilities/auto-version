#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/logging"

setup() {
  unset LOG_LEVEL
}

@test "$(basename ${BATS_TEST_FILENAME}) log_level_for defines error < warn < debug < info < unsupported" {
  source ${profile_script}
  assert [ $(log_level_for "error") -lt $(log_level_for "warn") ]
  assert [ $(log_level_for "warn") -lt $(log_level_for "debug") ]
  assert [ $(log_level_for "debug") -lt $(log_level_for "info") ]
  assert [ $(log_level_for "info") -gt $(log_level_for "anything else") ]
  assert [ $(log_level_for "anything else") -eq $(log_level_for "any unsupported") ]
}

@test "$(basename ${BATS_TEST_FILENAME}) current_log_level is -1 if LOG_LEVEL is not set" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  assert [ $(current_log_level) -eq -1 ]
}

@test "$(basename ${BATS_TEST_FILENAME}) current_log_level is log_level_for LOG_LEVEL if LOG_LEVEL is set" {
  for log_level in unsupported error warn debug info
  do
    export LOG_LEVEL=${log_level}
    assert [ $(current_log_level) -eq $(log_level_for "${LOG_LEVEL}") ]
  done
}

@test "$(basename ${BATS_TEST_FILENAME}) error does not print if log_level_for error is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "error") -le $(current_log_level) ]
  run error "${expected_message}"
  refute_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) error takes a string argument and prints it if log_level_for error is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "warn"; }

  assert [ $(log_level_for "error") -lt $(current_log_level) ]
  local expected_message="expected message"
  run error "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) error takes a string argument and prints it if log_level_for error equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "error"; }

  assert [ $(log_level_for "error") -eq $(current_log_level) ]
  local expected_message="expected message"
  run error "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) error prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="error"
  local expected_message="expected message"
  assert_equal "$(error "${expected_message}" > /dev/null)" "${actual_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) warn does not print if log_level_for warn is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "warn") -le $(current_log_level) ]
  run warn "${expected_message}"
  refute_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) warn takes a string argument and prints it if log_level_for warn is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "debug"; }

  assert [ $(log_level_for "warn") -lt $(current_log_level) ]
  local expected_message="expected message"
  run warn "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) warn takes a string argument and prints it if log_level_for warn equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "warn"; }

  assert [ $(log_level_for "warn") -eq $(current_log_level) ]
  local expected_message="expected message"
  run warn "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) warn prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="warn"
  local expected_message="expected message"
  assert_equal "$(warn "${expected_message}" > /dev/null)" "${actual_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) debug does not print if log_level_for debug is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "debug") -le $(current_log_level) ]
  run debug "${expected_message}"
  refute_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) debug takes a string argument and prints it if log_level_for debug is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "info"; }

  assert [ $(log_level_for "debug") -lt $(current_log_level) ]
  local expected_message="expected message"
  run debug "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) debug takes a string argument and prints it if log_level_for debug equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "debug"; }

  assert [ $(log_level_for "debug") -eq $(current_log_level) ]
  local expected_message="expected message"
  run debug "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) debug prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="debug"
  local expected_message="expected message"
  assert_equal "$(debug "${expected_message}" > /dev/null)" "${actual_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) info does not print if log_level_for info is not less than or equal to current_log_level" {
  source ${profile_script}
  assert_empty "${LOG_LEVEL}"
  local expected_message="expected message"

  refute [ $(log_level_for "info") -le $(current_log_level) ]
  run info "${expected_message}"
  refute_output "${expected_message}"
}

# this allows us to add new LOG_LEVEL support later that is even greater than info
@test "$(basename ${BATS_TEST_FILENAME}) info takes a string argument and prints it if log_level_for info is less than current_log_level" {
  source ${profile_script}

  function current_log_level() { echo 5; }

  assert [ $(log_level_for "info") -lt $(current_log_level) ]
  local expected_message="expected message"
  run info "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) info takes a string argument and prints it if log_level_for info equals current_log_level" {
  source ${profile_script}

  function current_log_level() { log_level_for "info"; }

  assert [ $(log_level_for "info") -eq $(current_log_level) ]
  local expected_message="expected message"
  run info "${expected_message}"
  assert_output "${expected_message}"
}

@test "$(basename ${BATS_TEST_FILENAME}) info prints to STDERR" {
  source ${profile_script}
  export LOG_LEVEL="info"
  local expected_message="expected message"
  assert_equal "$(info "${expected_message}" > /dev/null)" "${actual_message}"
}
