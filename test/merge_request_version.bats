#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/bin/merge_request_version"

setup() {
  export BOT_KEY="BOT_KEY"
  export CI_API_V4_URL="CI_API_V4_URL"
  export CI_PROJECT_ID="CI_PROJECT_ID"
  export CI_PROJECT_NAME='CI_PROJECT_NAME'
  export LOG_LEVEL="info"
}

@test "$(basename ${BATS_TEST_FILENAME}) auto_tag_label uses jq to get the first matching auto_tag label, if it exists, with an empty response if it does not" {
  source ${profile_script}

  function commit_merge_request_labels() { echo 'commit_merge_request_labels called' >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run auto_tag_label
  assert_success
  assert_output -p 'commit_merge_request_labels called'
  assert_output -p 'jq -r map(select(test("auto_tag")))[0] | select(.!=null)'
}

@test "$(basename ${BATS_TEST_FILENAME}) patch_level returns 1 with an error if auto_tag label is not present" {
  source ${profile_script}

  function auto_tag_label() { echo 'auto_tag_label called' >&2; }
  function merge_request_iid() { echo 'merge_request_iid called' >&2; echo 'merge_request_iid'; }
  run patch_level
  assert_failure
  assert_output -p 'auto_tag_label called'
  assert_output -p 'merge_request_iid called'
  assert_output -p 'supported auto_tag label not found in merge_request merge_request_iid'
}

@test "$(basename ${BATS_TEST_FILENAME}) patch_level returns patch increment from the auto_tag_label response" {
  source ${profile_script}

  local expected_tag='increment_tag'
  function auto_tag_label() { echo 'auto_tag_label called' >&2; echo 'auto_tag::increment_tag'; }
  function commit_merge_request_iid() { echo 'commit_merge_request_iid called' >&2; echo 'merge_request_iid'; }
  run patch_level
  assert_success
  assert_output -p 'auto_tag_label called'
  refute_output -p 'merge_request_iid called'
  assert_output -p "${expected_tag}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main checks required environment, gets the latest version, increments it, and creates a new tag prepended with 'v'" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function check_required_environment() { echo "check_required_environment ${*} called"; }
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  function issue_summary() { echo "issue_summary called" >&2; }
  function tag_version() { echo "tag_version ${*}" >&2; }

  run run_main
  assert_success
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "get_latest_version"
  assert_output -p "increment ${expected_current_version}"
  refute_output -p "issue_summary called"
  assert_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main adds addressed issues when --include-addressed-issues is passed" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function check_required_environment() { echo "check_required_environment ${*} called"; }
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  function issue_summary() { echo "issue_summary called" >&2; echo '- issue'; }
  function tag_version() { echo "tag_version ${*}" >&2; }

  run run_main '--include-addressed-issues'
  assert_success
  assert_output -p "check_required_environment ${expected_required_environment} called"
  assert_output -p "get_latest_version"
  assert_output -p "increment ${expected_current_version}"
  assert_output -p "issue_summary called"
  assert_output -p "tag_version ${expected_tag_version} issues addressed\n- issue"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main prints usage and exits when --usage is passed" {
  source ${profile_script}

  function check_required_environment() { echo "check_required_environment ${*} called"; }
  function get_latest_version() { echo "get_latest_version" >&2; }
  function increment() { echo "increment called" >&2; }
  function issue_summary() { echo "issue_summary called" >&2; }
  function tag_version() { echo "tag_version called" >&2; }
  function usage() { echo 'usage called' >&2; }

  run run_main '--usage'
  assert_success
  refute_output -p "check_required_environment ${expected_required_environment} called"
  refute_output -p "get_latest_version"
  refute_output -p "increment called"
  refute_output -p "issue_summary called"
  refute_output -p "tag_version called"
  assert_output -p "usage called"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if check_required_environment fails" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'
  local expected_required_environment="CI_PROJECT_NAME BOT_KEY CI_COMMIT_SHA CI_API_V4_URL CI_PROJECT_ID"

  function check_required_environment() { echo "check_required_environment ${*} called"; return 1; }
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  function tag_version() { echo "tag_version ${*}"; }

  run run_main
  assert_failure
  assert_output -p "check_required_environment ${expected_required_environment} called"
  refute_output -p "get_latest_version"
  refute_output -p "increment ${expected_current_version}"
  refute_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if increment fails" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'

  function check_required_environment() { echo "check_required_environment called"; }
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  function increment() { echo "increment ${*}" >&2; return 1; }
  function tag_version() { echo "tag_version ${*}"; }

  run run_main
  assert_failure
  assert_output -p "problem incrementing"
  assert_output -p "check_required_environment called"
  assert_output -p "get_latest_version"
  assert_output -p "increment ${expected_current_version}"
  refute_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) run_main fails if tag_version fails" {
  source ${profile_script}
  local expected_current_version='v0.0.1'
  local expected_tag_version='v0.0.2'

  function check_required_environment() { echo "check_required_environment ${*}"; }
  function get_latest_version() { echo "get_latest_version" >&2; echo "v0.0.1"; }
  function increment() { echo "increment ${*}" >&2; echo "0.0.2"; }
  function tag_version() { echo "tag_version ${*}"; return 1; }

  run run_main
  assert_failure
  assert_output -p "check_required_environment"
  assert_output -p "get_latest_version"
  assert_output -p "increment ${expected_current_version}"
  assert_output -p "tag_version ${expected_tag_version}"
}

@test "$(basename ${BATS_TEST_FILENAME}) usage prints supported arguments" {
 source ${profile_script}
 run usage
  echo "merge_request_version [flags]
optional flags supported:
--include-addressed-issues: changes tag message to a summary of issues addressed by the tag commit
" | assert_output 
}
