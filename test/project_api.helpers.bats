#!/usr/bin/env /usr/local/bin/bats
load '/usr/local/libexec/bats-support/load.bash'
load '/usr/local/libexec/bats-assert/load.bash'
load 'helpers'

profile_script="/usr/local/lib/helpers/project_api"

setup() {
  export LOG_LEVEL="info"
  export CI_API_V4_URL="CI_API_V4_URL"
  export CI_PROJECT_ID="CI_PROJECT_ID"
}

@test "$(basename ${BATS_TEST_FILENAME}) private_token returns value of PRIVATE_TOKEN" {
  source ${profile_script}

  export PRIVATE_TOKEN="PRIVATE_TOKEN"

  run private_token
  assert_output "${PRIVATE_TOKEN}"
}

@test "$(basename ${BATS_TEST_FILENAME}) private_token returns value of BOT_KEY" {
  source ${profile_script}

  export BOT_KEY="BOT_KEY"

  run private_token
  assert_output "${BOT_KEY}"
}

@test "$(basename ${BATS_TEST_FILENAME}) paginated returns false if NEXT_PAGE does not exist" {
  source ${profile_script}

  refute paginated
}

@test "$(basename ${BATS_TEST_FILENAME}) paginated returns false if NEXT_PAGE is empty" {
  source ${profile_script}

  export NEXT_PAGE=$(echo "foo" | grep "notfoo" )
  refute paginated
}

@test "$(basename ${BATS_TEST_FILENAME}) paginated returns true if NEXT_PAGE exists and is not empty" {
  source ${profile_script}

  export NEXT_PAGE=1
  assert paginated
}

@test "$(basename ${BATS_TEST_FILENAME}) paginated returns false if UNPAGINATED is set" {
  source ${profile_script}

  export UNPAGINATED=1
  refute paginated
}

@test "$(basename ${BATS_TEST_FILENAME}) paginated returns false if UNPAGINATED is set even if NEXT_PAGE exists and is not empty" {
  source ${profile_script}

  export NEXT_PAGE=1
  export UNPAGINATED=1
  refute paginated
}

@test "$(basename ${BATS_TEST_FILENAME}) force_unpaginated ensures all arguments are run with UNPAGINATED set" {
  source ${profile_script}

  function test_command() { echo "test_command ${*} UNPAGINATED ${UNPAGINATED}"; }

  run force_unpaginated test_command "arg1" "arg2"
  assert_output "test_command arg1 arg2 UNPAGINATED 1"
  refute [ -v UNPAGINATED ]
}

@test "$(basename ${BATS_TEST_FILENAME}) get takes path and makes private token authenticated gitlab project api get for path" {
  source ${profile_script}

  local expected_private_token='PT'
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run get ${expected_path}
  assert_output -p 'private_token called'
  assert_output -p "curl --include --silent -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) get returns json without headers" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_path="/path"

  export NEXT_PAGE=1
  function private_token() { echo "PT"; }
  function curl() { 
    cat <<EOF
content-type: application/json
x-next-page: 2
    
{"json":"true"}
EOF
  }

  run get ${expected_path}
  assert_output '{"json":"true"}'
}

@test "$(basename ${BATS_TEST_FILENAME}) get will set NEXT_PAGE to the value of the x-next-page response if paginated is true" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function paginated() { return; }
  function curl() { echo "curl ${*}" >&2; echo 'x-next-page: 2'; echo '{"json":"true"}'; }

  function test() {
    export NEXT_PAGE=''
    get ${expected_path} > /dev/null 2>&1
    echo ${NEXT_PAGE}
  }

  run test
  assert_success
  assert_output 2
}

@test "$(basename ${BATS_TEST_FILENAME}) get will set NEXT_PAGE even if x-next-page does not exist if paginated" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_path="/path"

  function paginated() { return; }

  function test() {
    export NEXT_PAGE=''
    get ${expected_path} > /dev/null 2>&1
    [ -z "${NEXT_PAGE}" ]
  }

  run test
  assert_success
}

@test "$(basename ${BATS_TEST_FILENAME}) put takes data and path and makes private token authenticated gitlab project api put to path" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_data="{\"key\":\"value\"}"
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run put "${expected_data}" "${expected_path}"
  assert_output -p "curl --silent -X PUT -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} -d ${expected_data} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) post takes data and path and makes private token authenticated gitlab project api post to path" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_data="{\"key\":\"value\"}"
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run post "${expected_data}" "${expected_path}"
  assert_output -p "curl --silent -X POST -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} -d ${expected_data} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) delete takes path and makes private token authenticated gitlab project api delete of the path" {
  source ${profile_script}
  local expected_private_token='PT'
  local expected_path="/path"

  function private_token() { echo 'private_token called' >&2; echo "PT"; }
  function curl() { echo "curl ${*}" >&2; }

  run delete ${expected_path}
  assert_output -p "curl --silent -X DELETE -H Content-Type: application/json -H Accept: application/json -H Private-Token: ${expected_private_token} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}${expected_path}"
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_requests calls gitlab api to get merge requests for CI_COMMIT_SHA by default" {
  source ${profile_script}
  export CI_COMMIT_SHA="CI_COMMIT_SHA"

  function get() { echo "get ${*}" >&2; echo 'merge response'; }
  run commit_merge_requests
  assert_success
  assert_output -p "get /repository/commits/${CI_COMMIT_SHA}/merge_requests"
  assert_output -p 'merge response'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_requests gets NEXT_PAGE of results if paginated" {
  source ${profile_script}
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  export NEXT_PAGE=1
  function get() { echo "get ${*}" >&2; echo 'merge response'; }
  run commit_merge_requests
  assert_success
  assert_output -p "get /repository/commits/${CI_COMMIT_SHA}/merge_requests?page=${NEXT_PAGE}"
  assert_output -p 'merge response'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_requests calls gitlab api to get merge requests for commit in first argument if provided" {
  source ${profile_script}
  export CI_COMMIT_SHA="CI_COMMIT_SHA"
  local expected_commit_sha="expected_commit_sha"

  function get() { echo "get ${*}" >&2; echo 'merge response'; }
  run commit_merge_requests "${expected_commit_sha}"
  assert_success
  refute_output -p "get /repository/commits/${CI_COMMIT_SHA}/merge_requests"
  assert_output -p "get /repository/commits/${expected_commit_sha}/merge_requests"
  assert_output -p 'merge response'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request uses jq to get the first object in unpaginated commit_merge_requests" {
  source ${profile_script}

  function force_unpaginated() { echo "force_unpaginated ${*}" >&2; ${*}; }
  function commit_merge_requests() { echo "commit_merge_requests ${*}" >&2; }
  function jq() { echo "jq ${*}" >&2; }

  run commit_merge_request
  assert_success
  assert_output -p 'force_unpaginated commit_merge_requests'
  assert_output -p 'commit_merge_requests'
  assert_output -p 'jq -r .[0]'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request passes first argument as desired_commit to commit_merge_requests if provided" {
  source ${profile_script}

  local expected_commit='expected_commit'
  function commit_merge_requests() { echo "commit_merge_requests ${*}" >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run commit_merge_request "${expected_commit}"
  assert_success
  assert_output -p "commit_merge_requests ${expected_commit}"
  assert_output -p 'jq -r .[0]'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_iid uses jq to get the iid of the commit_merge_request response" {
  source ${profile_script}

  function commit_merge_request() { echo "commit_merge_request ${*}" >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run commit_merge_request_iid
  assert_success
  assert_output -p 'commit_merge_request'
  assert_output -p 'jq -r .iid'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_iid passes first argument to commit_merge_request if provided" {
  source ${profile_script}

  local expected_commit='expected_commit'
  function commit_merge_request() { echo "commit_merge_request ${*}" >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run commit_merge_request_iid ${expected_commit}
  assert_success
  assert_output -p "commit_merge_request ${expected_commit}"
  assert_output -p 'jq -r .iid'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_labels calls commit_merge_request and uses jq to get the labels" {
  source ${profile_script}

  function commit_merge_request() { echo 'commit_merge_request called' >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run commit_merge_request_labels
  assert_success
  assert_output -p 'commit_merge_request called'
  assert_output -p 'jq .labels'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_labels passes first argument to commit_merge_request if provided" {
  source ${profile_script}

  local expected_commit='expected_commit'
  function commit_merge_request() { echo "commit_merge_request ${*}" >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run commit_merge_request_labels ${expected_commit}
  assert_success
  assert_output -p "commit_merge_request ${expected_commit}"
  assert_output -p 'jq .labels'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_description calls commit_merge_request and uses jq to get the description" {
  source ${profile_script}

  function commit_merge_request() { echo 'commit_merge_request called' >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run commit_merge_request_description
  assert_success
  assert_output -p 'commit_merge_request called'
  assert_output -p 'jq -r .description'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_description passes first argument to commit_merge_request if provided" {
  source ${profile_script}

  local expected_commit='expected_commit'
  function commit_merge_request() { echo "commit_merge_request ${*}" >&2; }
  function jq() { echo "jq ${*}" >&2; }
  run commit_merge_request_description ${expected_commit}
  assert_success
  assert_output -p "commit_merge_request ${expected_commit}"
  assert_output -p 'jq -r .description'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_issues calls commit_merge_request_description returns a Related Issue" {
  source ${profile_script}

  function commit_merge_request_description() { echo 'commit_merge_request_description called' >&2; echo 'Related to #4'; }
  run commit_merge_request_issues
  assert_success
  assert_output -p 'commit_merge_request_description called'
  assert_output -p '4'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_issues passes first argument to commit_merge_request_description if provided" {
  source ${profile_script}

  local expected_commit='expected_commit'
  function commit_merge_request_description() { echo "commit_merge_request_description ${*}" >&2; echo 'Related to #4'; }

  run commit_merge_request_issues ${expected_commit}
  assert_success
  assert_output -p "commit_merge_request_description ${expected_commit}"
  assert_output -p '4'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_issues calls handles multiple Related Issues on the same line" {
  source ${profile_script}

  function commit_merge_request_description() { echo 'commit_merge_request_description called' >&2; echo 'Related to #4 #5'; }
  run commit_merge_request_issues
  assert_success
  assert_output -p 'commit_merge_request_description called'
  assert_output -p '4 5'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_issues calls commit_merge_request_description returns a Closes Issue" {
  source ${profile_script}

  function commit_merge_request_description() { echo 'commit_merge_request_description called' >&2; echo 'Closes #4'; }
  run commit_merge_request_issues
  assert_success
  assert_output -p 'commit_merge_request_description called'
  assert_output -p '4'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merge_request_issues handles multiple Closes Issues on the same line" {
  source ${profile_script}

  function commit_merge_request_description() { echo 'commit_merge_request_description called' >&2; echo 'Closes #4 #5'; }
  run commit_merge_request_issues
  assert_success
  assert_output -p 'commit_merge_request_description called'
  assert_output -p '4 5'
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_summary processes commit_merge_request_issues into a summary and list of issue number and title entries" {
  source ${profile_script}

  function commit_merge_request_issues() { echo 'commit_merge_request_issues called' >&2; echo '1 2'; }
  function get() { echo "get ${*}" >&2; if [ "${1}" == "/issues/1" ]; then echo '{"iid":1,"title":"first issue"}'; else echo '{"iid":2,"title":"second issue"}'; fi; }
  run issue_summary
  assert_success
  assert_output -p 'commit_merge_request_issues called'
  assert_output -p 'get /issues/1'
  assert_output -p 'get /issues/2'
  assert_output -p '- #1 first issue\n- #2 second issue'
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_summary passes first argument to commit_merge_request_issues if provided" {
  source ${profile_script}

  local expected_commit='expected_commit'
  function commit_merge_request_issues() { echo "commit_merge_request_issues ${*}" >&2; echo '1 2'; }
  function get() { echo "get ${*}" >&2; if [ "${1}" == "/issues/1" ]; then echo '{"iid":1,"title":"first issue"}'; else echo '{"iid":2,"title":"second issue"}'; fi; }

  run issue_summary ${expected_commit}
  assert_success
  assert_output -p "commit_merge_request_issues ${expected_commit}"
  assert_output -p 'get /issues/1'
  assert_output -p 'get /issues/2'
  assert_output -p '- #1 first issue\n- #2 second issue'
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merged_at returns the merged_at date for a commit_merge_request" {
  source ${profile_script}

  local expected_merged_at="expected_merged_at_date"
  function commit_merge_request() { echo '{"merged_at":"expected_merged_at_date"}'; }

  run commit_merged_at
  assert_success
  assert_output "${expected_merged_at}"
}

@test "$(basename ${BATS_TEST_FILENAME}) commit_merged_at passes first argument to commit_merge_request if provided" {
  source ${profile_script}

  local expected_commit='expected_commit'
  local expected_merged_at="expected_merged_at_date"
  function commit_merge_request() { echo "commit_merge_request ${*}" >&2; echo '{"merged_at":"expected_merged_at_date"}'; }

  run commit_merged_at ${expected_commit}
  assert_success
  assert_output -p "commit_merge_request ${expected_commit}"
  assert_output -p "${expected_merged_at}"
}

@test "$(basename ${BATS_TEST_FILENAME}) label_issue takes an issue id and label and applies the label to the issue" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'
  local expected_label='label'

  function put() { echo "put ${*}"; }

  run label_issue "${expected_issue_id}" "${expected_label}"
  assert_success
  assert_output "put {\"add_labels\":\"${expected_label}\"} /issues/${issue_iid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) label_commit_merge_request_issues takes a label and applies it to all commit_merge_request_issues" {
  source ${profile_script}

  local expected_label='label'
  function commit_merge_request_issues() { echo '1 2'; }
  function label_issue() { echo "label_issue ${*} called"; }

  run label_commit_merge_request_issues "${expected_label}"
  assert_success
  assert_output -p "label_issue 1 ${expected_label}"
  assert_output -p "label_issue 2 ${expected_label}"
}

@test "$(basename ${BATS_TEST_FILENAME}) label_commit_merge_request_issues passes second argument to commit_merge_request_issues if provided" {
  source ${profile_script}

  local expected_label='label'
  local expected_commit='expected_commit'
  function commit_merge_request_issues() { echo "commit_merge_request_issues ${*}" >&2; echo '1 2'; }
  function label_issue() { echo "label_issue ${*} called"; }

  run label_commit_merge_request_issues "${expected_label}" ${expected_commit}
  assert_success
  assert_output -p "commit_merge_request_issues ${expected_commit}"
  assert_output -p "label_issue 1 ${expected_label}"
  assert_output -p "label_issue 2 ${expected_label}"
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_merge_requests returns any merge requests related to an issue" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'

  function get() { echo "get ${*}"; }

  run issue_merge_requests "${expected_issue_iid}"
  assert_success
  assert_output "get /issues/${expected_issue_iid}/related_merge_requests"
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_merge_requests gets NEXT_PAGE of results if paginated" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'
  export NEXT_PAGE=1

  function get() { echo "get ${*}"; }

  run issue_merge_requests "${expected_issue_iid}"
  assert_success
  assert_output "get /issues/${expected_issue_iid}/related_merge_requests?page=${NEXT_PAGE}"
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_merge_count_on_or_before_commit returns the number of merged issue_merge_requests that were merged on or before the current commitment" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'
  local expected_count="2"

  function issue_merge_requests() { echo "issue_merge_requests ${*} called" >&2; echo '[{"state":"draft"},{"state":"merged","merged_at":"04-24-2023"},{"state":"merged","merged_at":"04-23-2023"},{"state":"merged","merged_at":"04-22-2023"}]'; }
  function commit_merged_at() { echo "04-23-2023"; }

  run issue_merge_count_on_or_before_commit "${expected_issue_iid}"
  assert_success
  assert_output -p "issue_merge_requests ${expected_issue_iid}"
  assert_output -p ${expected_count}
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_merge_count_on_or_before_commit passes second argument to commit_merged_at if provided" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'
  local expected_commit='expected_commit'
  local expected_count="2"

  function issue_merge_requests() { echo "issue_merge_requests ${*} called" >&2; echo '[{"state":"draft"},{"state":"merged","merged_at":"04-24-2023"},{"state":"merged","merged_at":"04-23-2023"},{"state":"merged","merged_at":"04-22-2023"}]'; }
  function commit_merged_at() { echo "commit_merged_at ${*}" >&2; echo "04-23-2023"; }

  run issue_merge_count_on_or_before_commit "${expected_issue_iid}" ${expected_commit}
  assert_success
  assert_output -p "commit_merged_at ${expected_commit}"
  assert_output -p "issue_merge_requests ${expected_issue_iid}"
  assert_output -p ${expected_count}
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_has_merges_on_or_before_commit returns true if the count is greater than 0" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'
  function issue_merge_count_on_or_before_commit() { echo "1"; }

  assert issue_has_merges_on_or_before_commit "${expected_issue_iid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_has_merges_on_or_before_commit returns false if the count is 0" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'
  function issue_merge_count_on_or_before_commit() { echo "0"; }

  refute issue_has_merges_on_or_before_commit "${expected_issue_iid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) issue_has_merges_on_or_before_commit passes second argument to issue_merge_count_on_or_before_commit if provided" {
  source ${profile_script}

  local expected_issue_iid='issue_iid'
  local expected_commit='expected_commit'
  function issue_merge_count_on_or_before_commit() { echo "issue_merge_count_on_or_before_commit ${*}" >&2; echo "1"; }

  run issue_has_merges_on_or_before_commit "${expected_issue_iid}" ${expected_commit}
  assert_output -p "issue_merge_count_on_or_before_commit ${expected_issue_iid} ${expected_commit}"
}

@test "$(basename ${BATS_TEST_FILENAME}) only_issues_merged_on_or_before_commit takes a list of issue ids and returns only issues that have merges on or before the current commit" {
  source ${profile_script}

  local expected_issue_iids='1 2 3 4'
  function issue_has_merges_on_or_before_commit() { [ "${1}" -gt 2 ]; }

  run only_issues_merged_on_or_before_commit "${expected_issue_iids}"
  assert_output "3 4"
}

@test "$(basename ${BATS_TEST_FILENAME}) only_issues_merged_on_or_before_commit passes the second argument to issue_has_merges_on_or_before_commit if provided" {
  source ${profile_script}

  local expected_issue_iids='1 2 3 4'
  local expected_commit='expected_commit'
  function issue_has_merges_on_or_before_commit() { echo "issue_has_merges_on_or_before_commit ${*}" >&2; [ "${1}" -gt 2 ]; }

  run only_issues_merged_on_or_before_commit "${expected_issue_iids}" "${expected_commit}"
  assert_output -p "issue_has_merges_on_or_before_commit 1 ${expected_commit}"
  assert_output -p "issue_has_merges_on_or_before_commit 2 ${expected_commit}"
  assert_output -p "issue_has_merges_on_or_before_commit 3 ${expected_commit}"
  assert_output -p "issue_has_merges_on_or_before_commit 4 ${expected_commit}"
  assert_output -p "3 4"
}

@test "$(basename ${BATS_TEST_FILENAME}) issues_deployed_to takes an environment and returns the issues deployed to that environment" {
  source ${profile_script}

  local expected_environment='productiony'
  local expected_issues="3 4"
  function get() { echo "get ${*}" >&2; echo '[{"iid":"3"},{"iid":"4"}]'; }

  run issues_deployed_to "${expected_environment}"
  assert_output -p "get /issues?labels=deployed::${expected_environment}&state=opened"
  assert_output -p "3"
  assert_output -p "4"
}

@test "$(basename ${BATS_TEST_FILENAME}) issues_deployed_to gets NEXT_PAGE results if paginated" {
  source ${profile_script}

  local expected_environment='productiony'
  local expected_issues="3 4"
  export NEXT_PAGE=1
  function paginated() { return; }
  function get() { echo "get ${*}" >&2; echo '[{"iid":"3"},{"iid":"4"}]'; }

  run issues_deployed_to "${expected_environment}"
  assert_output -p "get /issues?page=${NEXT_PAGE}&labels=deployed::${expected_environment}&state=opened"
  assert_output -p "3"
  assert_output -p "4"
}

@test "$(basename ${BATS_TEST_FILENAME}) label_deployment_issues takes from_environment and to_environment and applies a deployed::to_environment label to all issues that are deployed::from_environment merged on or before the current commit" {
  source ${profile_script}

  local expected_from_environment='stagingy'
  local expected_to_environment='productiony'
  local expected_issues_deployed_to_from_environment='1 2 3 4'
  local expected_merged_on_or_before_issues='1 2'
  function only_issues_merged_on_or_before_commit() { echo "only_issues_merged_on_or_before_commit ${*}" >&2; echo '1 2'; }
  function issues_deployed_to() { echo "issues_deployed_to ${*}" >&2; echo '1 2 3 4'; }
  function label_issue() { echo "label_issue ${*}" >&2; }

  run label_deployment_issues "${expected_from_environment}" "${expected_to_environment}"
  assert_success
  assert_output -p "only_issues_merged_on_or_before_commit ${expected_issues_deployed_to_from_environment}"
  assert_output -p "issues_deployed_to ${expected_from_environment}"
  assert_output -p "label_issue 1 deployed::${expected_to_environment}"
  assert_output -p "label_issue 2 deployed::${expected_to_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME}) label_deployment_issues passes third argument to only_issues_merged_on_or_before_commit if provided" {
  source ${profile_script}

  local expected_from_environment='stagingy'
  local expected_to_environment='productiony'
  local expected_issues_deployed_to_from_environment='1 2 3 4'
  local expected_merged_on_or_before_issues='1 2'
  local expected_commit='expected_commit'
  function only_issues_merged_on_or_before_commit() { echo "only_issues_merged_on_or_before_commit ${*}" >&2; echo '1 2'; }
  function issues_deployed_to() { echo "issues_deployed_to ${*}" >&2; echo '1 2 3 4'; }
  function label_issue() { echo "label_issue ${*}" >&2; }

  run label_deployment_issues "${expected_from_environment}" "${expected_to_environment}" "${expected_commit}"
  assert_success
  assert_output -p "only_issues_merged_on_or_before_commit ${expected_issues_deployed_to_from_environment} ${expected_commit}"
  assert_output -p "issues_deployed_to ${expected_from_environment}"
  assert_output -p "label_issue 1 deployed::${expected_to_environment}"
  assert_output -p "label_issue 2 deployed::${expected_to_environment}"
}

@test "$(basename ${BATS_TEST_FILENAME}) close_issue closes the provided issued_id issue" {
  source ${profile_script}

  local expected_issue_iid='1'
  function put() { echo "put ${*}" >&2; }
  run close_issue "${expected_issue_iid}"
  assert_success
  assert_output "put {\"state_event\":\"close\"} /issues/${expected_issue_iid}"
}

@test "$(basename ${BATS_TEST_FILENAME}) close_production_issues closes all open issues that are deployed::production" {
  source ${profile_script}

  local expected_environment='production'
  function issues_deployed_to() { echo "issues_deployed_to ${*}" >&2; echo '1 2'; }
  function close_issue() { echo "close_issue ${*}" >&2; }

  run close_production_issues
  assert_success
  assert_output -p "issues_deployed_to ${expected_environment}"
  assert_output -p "close_issue 1"
  assert_output -p "close_issue 2"
}

@test "$(basename ${BATS_TEST_FILENAME}) commits returns all commits for the project" {
  source ${profile_script}

  function get() { echo "get ${*}"; }
  run commits
  assert_output 'get /repository/commits'
}

@test "$(basename ${BATS_TEST_FILENAME}) commits returns NEXT_PAGE results if paginated" {
  source ${profile_script}

  export NEXT_PAGE=1
  function get() { echo "get ${*}"; }
  function paginated() { return; }
  run commits
  assert_output "get /repository/commits?page=${NEXT_PAGE}"
}

@test "$(basename ${BATS_TEST_FILENAME}) commit returns data for CI_COMMIT_SHA" {
  source ${profile_script}

  export CI_COMMIT_SHA='CI_COMMIT_SHA'
  function get() { echo "get ${*}"; }
  run commit
  assert_output "get /repository/commits/${CI_COMMIT_SHA}"
}

@test "$(basename ${BATS_TEST_FILENAME}) commit returns data for first argument if specified" {
  source ${profile_script}

  export CI_COMMIT_SHA='CI_COMMIT_SHA'
  local expected_commit='expected_commit'
  function get() { echo "get ${*}"; }
  run commit ${expected_commit}
  refute_output "get /repository/commits/${CI_COMMIT_SHA}"
  assert_output "get /repository/commits/${expected_commit}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tags returns all tags for the project" {
  source ${profile_script}

  function get() { echo "get ${*}"; }
  run tags
  assert_output 'get /repository/tags'
}

@test "$(basename ${BATS_TEST_FILENAME}) tags returns NEXT_PAGE results if paginated" {
  source ${profile_script}

  export NEXT_PAGE=1
  function get() { echo "get ${*}"; }
  run tags
  assert_output "get /repository/tags?page=${NEXT_PAGE}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag returns data for CI_COMMIT_TAG" {
  source ${profile_script}

  export CI_COMMIT_TAG='CI_COMMIT_TAG'
  function get() { echo "get ${*}"; }
  run tag
  assert_output "get /repository/tags/${CI_COMMIT_TAG}"
}

@test "$(basename ${BATS_TEST_FILENAME}) tag returns data for first argument if specified" {
  source ${profile_script}

  export CI_COMMIT_TAG='CI_COMMIT_TAG'
  local expected_tag='expected_tag'
  function get() { echo "get ${*}"; }
  run tag ${expected_tag}
  refute_output "get /repository/tags/${CI_COMMIT_TAG}"
  assert_output "get /repository/tags/${expected_tag}"
}

@test "$(basename ${BATS_TEST_FILENAME}) latest_release gets the latest release" {
  source ${profile_script}

  function force_unpaginated() { echo "force_unpaginated ${*}" >&2; ${*}; }
  function get() { echo '[{"tag_name":"latest"},{"tag_name":"penultimate"}]'; }

  run latest_release
  assert_output -p 'force_unpaginated get /releases?order_by=released_at&sort=desc'
  assert_output -p '"tag_name": "latest"'
  refute_output -p '"tag_name": "penultimate"'
}

@test "$(basename ${BATS_TEST_FILENAME}) releases returns all releases" {
  source ${profile_script}

  function get() { echo "get ${*}"; }

  run releases
  assert_output 'get /releases'
}

@test "$(basename ${BATS_TEST_FILENAME}) releases supports pagination" {
  source ${profile_script}

  export NEXT_PAGE=1
  function get() { echo "get ${*}"; }

  run releases
  assert_output "get /releases?page=${NEXT_PAGE}" 
}

@test "$(basename ${BATS_TEST_FILENAME}) release returns release for given tag_name" {
  source ${profile_script}

  local expected_tag='expected_tag'
  function get() { echo "get ${*}"; }

  run release ${expected_tag}
  assert_output "get /releases/${expected_tag}"
}

@test "$(basename ${BATS_TEST_FILENAME}) latest_unreleased_tags returns names from up to 100 tags created after the latest_release" {
  source ${profile_script}

  function latest_release() { echo 'latest_release called' >&2; echo '{"tag_name":"released_tag"}'; }
  function force_unpaginated() { echo "force_unpaginated ${*}" >&2; ${*}; }
  function get() { echo '[{"name":"one"},{"name":"two"},{"name":"three","release":{"tag_name":"released_tag"}},{"name":"four"}]'; }

  run latest_unreleased_tags
  assert_output -p 'latest_release called'
  assert_output -p 'force_unpaginated get /repository/tags?page=1&per_page=100&order_by=updated&sort=desc'
  assert_output -p 'one'
  assert_output -p 'two'
  refute_output -p 'three'
  refute_output -p 'four'
}

@test "$(basename ${BATS_TEST_FILENAME}) latest_unreleased_tags returns all tags if no release exists" {
  source ${profile_script}

  function latest_release() { echo 'latest_release called' >&2; echo '{"error":"404 Not Found"}'; }
  function force_unpaginated() { echo "force_unpaginated ${*}" >&2; ${*}; }
  function get() { echo '[{"name":"one"},{"name":"two"},{"name":"three"},{"name":"four"}]'; }

  run latest_unreleased_tags
  assert_output -p 'latest_release called'
  assert_output -p 'force_unpaginated get /repository/tags?page=1&per_page=100&order_by=updated&sort=desc'
  assert_output -p 'one'
  assert_output -p 'two'
  assert_output -p 'three'
  assert_output -p 'four'
}

@test "$(basename ${BATS_TEST_FILENAME}) latest_unreleased_tags allows per_page override" {
  source ${profile_script}

  function latest_release() { echo '{"tag_name":"released_tag"}'; }
  function force_unpaginated() { echo "force_unpaginated ${*}" >&2; ${*}; }
  function get() { echo '[{"name":"one"},{"name":"two"},{"name":"three","release":{"tag_name":"released_tag"}},{"name":"four"}]'; }

  run latest_unreleased_tags 500
  assert_output -p 'force_unpaginated get /repository/tags?page=1&per_page=500&order_by=updated&sort=desc'
  assert_output -p 'one'
  assert_output -p 'two'
  refute_output -p 'three'
  refute_output -p 'four'
}

@test "$(basename ${BATS_TEST_FILENAME}) release_latest_tag creates a release with an issue_summary for CI_COMMIT_TAG and all other unreleased_tags" {
  source ${profile_script}

  export CI_COMMIT_TAG='ci_commit_tag'
  export CI_COMMIT_SHA='ci_commit_tag_commit'
  local expected_description="${CI_COMMIT_TAG}:\n\n${CI_COMMIT_TAG}_commit_issue_summary\n\ntwo:\n\ntwo_commit_issue_summary\n\nthree:\n\nthree_commit_issue_summary\n\nfour:\n\nfour_commit_issue_summary"
  local expected_data="{\"tag_name\":\"${CI_COMMIT_TAG}\",\"description\":\"${expected_description}\"}";
  function latest_unreleased_tags() { echo 'two three four'; }
  function issue_summary() { echo "${1}_issue_summary"; }
  function tag() { echo '{"name":"'${1}'","commit":{"id":"'${1}'_commit"}}'; }
  function post() { echo "post ${*}"; }

  run release_latest_tag
  assert_output "post ${expected_data} /releases" 
}
