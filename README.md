# auto-version

Utility image and gitlab ci extensions to automate the use of semantic versioning in project build pipelines

Image Version: v2.4.1

### General Usage

This utility automates the process of incrementing [semantic versions](https://semver.org/)
in your project. This is done by using git tags of the project.

bin/auto_version script scans the tags for the latest tag that has been applied matching a semantic 
version expression `^v[0-9]+\.[0-9]+\.[0-9]+$`.

bin/auto_tag works like bin/auto_version, but uses a different syntax in the commit message (see below).
If the commit message pattern is not found, it will not create any new tag in the repo, but will exit
successfully.

bin/merge_request_version works like bin/auto_tag. Instead of looking for an expression
in the commit message, it uses a label on the first merge_request for the current commit that
looks like `auto_tag::$increment`, where $increment is one of the supported patch levels. If
the merge_request does not have a supported auto_tag label, this job exits nonzero. You can
add a label to the closed merge_request and rerun the job to fix the issue.

For all three of these, if a semantic version tag has not been created in the project, the first version will be created by incrementing v0.0.0 with the desired semver version  level (see below).

bin/trigger_tag_deployment requires CI_ENVIRONMENT_NAME and CI_COMMIT_TAG, and triggers a pipeline
on the tag with DEPLOY_TARGET set to CI_ENVIRONMENT_NAME

#### semver patch levels

Semantic versions can be incremented in the following ways:
- major: when you make incompatible API changes
- minor: when you add functionality in a backwards compatible manner
- patch: when you make backwards compatible bug fixes
- prepatch: when you are creating a candidate release tag for a future patch,
  e.g. if latest tag is v1.2.4, prepatch yields v1.2.5-0
- preminor: when you are creating a candidate release for a future minor,
  e.g. preminor v1.3.23 yields v1.4.0-0
- premajor: when you are creating a candidate release for a future major,
  e.g. premajor v1.3.23 yields v2.0.0-0
- prerelease: take an existing candidate release tag (regardless of type) and increment
  the release candidate version, e.g. if a tag v1.2.5-0 exists, prerelease yields v1.2.5-1.
  prerelease v1.2.5-10 yields v1.2.5-11, etc.

### semver release candidate lifecycle

If you want to be intentional about creating release candidates for future versions, semver allows 
the use of any of the 4 pre-* patch_levels above. The lifecycle of a patch release of v1.3.4 might look like:

semver -i prepatch v1.3.4 -> v1.3.5-0
semver -i prerelease v1.3.5-0 -> v1.3.5-1
semver -i prerelease v1.3.5-1 -> v1.3.5-2
semver -i patch v1.3.5-2 -> v1.3.5

note that the following return the exact same values:
semver -i patch v1.3.5-2 -> v1.3.5
semver -i patch v1.3.4 -> v1.3.5

semver allows you to specify a 'preid' to make your versions a little more informative. It is
highly recommended that you use a preid in your release candidates. One use of 'preid' is to add
a jira issue (e.g. ir-433) to make your tags reflect the jira
issue going into the future release:

semver -i prepatch --preid ir-433 v1.3.4 -> v1.3.5-ir-433.0
semver -i prerelease --preid ir-433 v1.3.5-ir-433.0 -> v1.3.5-ir-433.1
semver -i patch --preid ir-433 v1.3.5-ir-433.1 -> v1.3.5

note that the following return the exact same values:
semver -i patch --preid ir-433 v1.3.5-ir-433.1 -> v1.3.5
semver -i patch --preid ir-433 v1.3.4 -> v1.3.5

## Using auto-version.yml in your project .gitlab-ci.yml
 
Include a specific tag of the auto-version.yml using the following in the base of
your .gitlab-ci.yml (or whatever you use) file:

```
include:
- project: 'utility/images/auto-version'
  file: '/auto-version.yml'
  ref: 'v2.4.1'
```

notes
- if you have already included other projects, just add another entry
- you may want to ensure you are using the latest version of the auto-version.yml in the ref

There are three possible modes of usage:
- auto_version: legacy auto_version system that uses SEMVER_PATCH_LEVEL
- auto_tag: newer system that allows an optional release to be made from a COMMIT
- merge_request_version: newest version that uses a merge_request label instead of the COMMIT to
drive semantic versions. If the label is missing, the job will fail.

### auto_version

auto_version is intended for simple, automated tagging of versions. It does not support
the generation and management of release_candidates.

extend the .auto_version function in one job in your pipeline.

```
increment_version:
  extends: .auto_version
  stage: publish
  only:
    - master
```

#### Set Required ENVIRONMENT

This utility must have certain environment variables defined when it
runs. Some of these will be supplied by gitlab ci, others
must be created in the gitlab CI/CD Environment Variables:

- BOT_KEY: personal access token for a user with the ability to create
tags in your project, or project_access_token with api scope
- CI_API_V4_URL: The full url to the gitlab api, ending with /api/v4.

#### Action

This always creates a new tag when it is run. It's default is to apply a patch increment to
the last tag that is in the repository (starting with v0.0.0 if this is the first tag being created in
a repository). You can override the patch_level to a supported level (see above, but do not use pre-* levels)
by adding a line to your commit with:

```
SEMVER_VERSION_LEVEL:${level}
```

Caveats:
- do not override the job `image` unless you have to (see Using an Older Version)
- do not override the job `script` section
- do not override the job `tag`
- it does not make sense to run this job more than once in a build
pipeline

### auto_tag

auto_tag should be used if you plan to be very intentional about your tags and releases. It allows
you to make use of all semver's features, including release candidate generation. auto_tag requires the
use of preid when generating and managing release candidate tags.
extend the .auto_tag function in one job in your pipeline.

```
increment_version:
  extends: .auto_tag
  stage: publish
  only:
    - master
```
#### Set Required ENVIRONMENT

This utility must have certain environment variables defined when it
runs. Some of these will be supplied by gitlab ci, others must be
created in the gitlab CI/CD Environment Variables:

- BOT_KEY: personal access token for a user with the ability to create
tags in your project, or project_access_token with api scope
- CI_API_V4_URL: The full url to the gitlab api, ending with /api/v4. This is typically supplied by gitlab ci

#### Action

This will not create a new tag in the repository unless the one of two conditions is met:

- the commit contains a line with an auto_tag block at the end '[auto_tag options]':
```
[auto_tag patch]: take the latest vX.Y.Z tag, patch increment, and create a new tag
[auto_tag minor @v3]: take the latest v3.Y.Z tag, minor increment, and create a new tag
[auto_tag major release!]: take the latest vX.Y.Z tag, major increment it, and create a release using the rest
of the commitment message (minus the [auto_tag] block as the description, and the tag as name)
[auto_tag prepatch ir-433]: take the latest VX.Y.Z tag, prepatch increment it with preid ir-433
[auto_tag prerelease ir-433]: take the latest VX.Y.Z-ir-433.Q tag, increment the release candidate number Q, and
create a new tag
```

- a Variable AUTO_TAG is present with supported options
```
AUTO_TAG=patch
AUTO_TAG=patch release!
...
```

The auto_tag options look like the following:
increment [preid] [modifier]

- increment: required with every auto_tag block. Can accept one of the supported patch_levels (see above).
Can also accept `release!`. If the increment is not present, or not a supported patch_level or `release!`
the auto_tag job will fail. If `release!` then a release is made of the latest vX.Y.Z tag present, without
incrementing it (see Release Commits below).

- preid: if increment is one of the 4 release candidate patch_levels (prepatch, preminor, premajor, prerelease),
the second string in the auto_tag block must be a preid, or the auto_tag job will fail. All future "prerelease" increments of that release candidate must include the same preid. The increment cannot be "prerelease", if you have not already run a job with increment of prepatch, preminor, or premajor in a previous auto_tag run (e.g. there would already exist a tag vX.Y.Z-${preid}.Q in your repo to increment the prerelease on), your job will fail. Running a patch, minor, or major increment will always increment the latest tag with vX.Y.Z, and ignore all release_candidate vX.Y.C-${preid}.Q tags that exist.

- modifier: can be one of the following:
  - release!: This modifier will be ignored for any release candidate patch_level starting with `pre`. 
  This takes the newly created vX.Y.Z tag after it has been incremented, and creates a new release of it (see 
  Release Commits below).
  - @vM: this makes auto_tag use the latest vM.Y.Z (e.g. major version) for its actions instead of
  the absolute latest vX.Y.Z tag. Note, if @vM is present, the major or premajor patch_levels are not allowed,
  and will cause the auto_tag job to fail. This is for cases where multiple repository branches are kept to
  track tags/releases of multiple major version lineages of the same application, api, or library, each with
  its own release history.

Caveats:
- do not override the job `image` unless you have to (see Using an Older Version)
- do not override the job `script` section
- do not override the job `tag`
- it does not make sense to run this job more than once in a build
pipeline

#### Release Commits

If your auto_tag block includes `release!` then the entire commit message for the commit being tagged
will be stripped of the [auto_tag options] block, and used as the description for the new release, and
the tag name will be used as the release name.

One drawback of auto_tag is that a failed build is not easy to fix without a completely new commit to
the repository.

### merge_request_version

merge_request_version can also be used if you plan to be intentional about your tags and releases. It does not currently support pre releases.
extend the .merge_request_version function in one job in your pipeline.

```
increment_version:
  extends: .merge_request_version
  stage: publish
  only:
    - master
```
#### Set Required ENVIRONMENT

This utility must have certain environment variables defined when it
runs. Some of these will be supplied by gitlab ci, others must be
created in the gitlab CI/CD Environment Variables:

- BOT_KEY: personal access token for a user with the ability to create
tags in your project, or project_access_token with api scope
- CI_API_V4_URL: The full url to the gitlab api, ending with /api/v4. This is typically supplied by gitlab ci

Allows the following optional Variables (set in before_script or as CI/CD variable):
- merge_request_arguments: can be set to '--include-addressed-issues' to include the issues addressed by the tagged commit merge request in the tag message. If not set, the message will be
${new_version} generated by ${BOT_NAME:-semver} bot", where new_version is the new tag name to be created. BOT_NAME can also be set in Variables to override the name of the bot that generated the tag.

#### Action

This will only create a new tag in the repository if the first merge_request associated with the
current commit has a supported auto_tag label of the form `auto_tag::$increment`, where $increment
can be one of:
- patch
- minor
- major

If the merge request does not contain an auto_tag label, the job will fail. You can add a label to
the closed merge request and rerun the job to fix build.

At this time, pre release increments are not supported by merge_request_version.

### trigger_tag_deployment

Call this in a tag pipeline job with a TRIGGER_DEPLOY_TARGET variable to trigger a child pipeline on the tag

```
trigger_staging:
  extends: .trigger_tag_deployment
  variables:
    TRIGGER_DEPLOY_TARGET: staging
  stage: deploy
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$/ && $CI_PIPELINE_SOURCE != "pipeline"'
    - if: '$CI_COMMIT_BRANCH'
      when: never

trigger_production:
  extends: .trigger_tag_deployment
  stage: deploy
  variables:
    TRIGGER_DEPLOY_TARGET: production
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*$/ && $CI_PIPELINE_SOURCE != "pipeline" && $DEPLOY_TARGET == "staging"'
    - if: '$CI_COMMIT_BRANCH'
      when: never
```
## project_api

This image includes a project_api helper that can be used on its own in a variety
of ways to interact with the gitlab api to get, post, put, and delete project objects.

To use the library, you must set the following environment variables:

- CI_API_V4_URL *: The url to the gitlab instance being accessed with /api/v4 appended. example: https://gitlab.dhe.duke.edu/api/v4.
- CI_PROJECT_ID *: can either be the integer Project ID (look for this on the main page of your repo in gitlab), or the url encoded path name with group or netid.
- private_token: either PRIVATE_TOKEN (preferred), or BOT_KEY (legacy)project access token, group access token, or personal access token with api access to the CI_PROJECT_ID project.
* set by gitlab when used in a gitlab job

examples:
```
source lib/helpers/project_api
get /commits
get /tags
get /tags/$tag_name
get /releases/$tag_name
```

The api has limited support for pagination on index calls, but this may fail for
advanced uses, such as subshell expansion of variables:
```
export NEXT_PAGE=1
while paginated
do
  commits > out.${NEXT_PAGE}
end
```

## Using an Older Version in your CI Build

In most cases, you will not want to override the job `image`. If you
are experiencing problems with the auto-version image, you can try
reverting to a [previously built semantic version](https://gitlab.dhe.duke.edu/utility/images/auto-version/container_registry) by setting image to:

```
image: $CI_REGISTRY/utility/images/auto-version:$VERSION
```

auto_tag was introduced in v1.0.4
merge_request_version was introduced in v2.1.1
trigger_tag_deployment was introduced in v2.2.0

## Improving this image
To get involved improving this image, consult CONTRIBUTING.md.

Note, you should always leave the ref of the auto_version import in .gitlab-ci.yml set to a
previously created version tag until after an auto_tag is actually run to create the next version tag.
You can then commit a change to the .gitlab-ci.yml to make the value of that ref match the value in
VERSION_TAG. This will rebuild the same version of the image.
