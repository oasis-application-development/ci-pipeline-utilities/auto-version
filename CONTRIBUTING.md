Contributers should fork this project into their personal project, make
changes, test them, commit them and push the changes to your
personal fork, then create a Merge Request of the changes back
to the upstream project master branch, and request a maintainer
(see Maintainers.md) to review the merge request.

### Build Process
The Dockerfile is built into a candidate iamge using the kaniko_builder
in utility/project-templates/ci-templates

It must add bin, all directories in libexec, and test to
the resulting image.

Each change MUST increment the VERSION_TAG in .gitlab-ci.yml.

The build pipeline tests the candidate image with bats. If this
test fails, it will not proceed to tag the candidate iamge with the latest version tag, or latest. If the test passes, the candidate image
is tagged with the VERSION_TAG, and latest.

autoversion.yml should always refer to :latest. This ensures that it will
always work in the event of a build failure.

### Dependencies

This uses the following features of of the shared gitlab.dhe.duke.edu/utility/project-templates/ci-templates project docker.yml:
- .kaniko_build
- .registry_image_tag

