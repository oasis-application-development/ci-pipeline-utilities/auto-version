FROM image-mirror-prod-registry.cloud.duke.edu/library/ubuntu:22.04

ENV AUTOVERSION_VERSION=v2.4.1

#ARG NODEJS_VERSION=v18.0.0
ARG NODEJS_VERSION=v18.12.1

RUN apt update -y && apt upgrade -y \
    && apt install -y curl gcc g++ gettext-base jq make xz-utils wget

RUN cd /tmp \
    && curl -L https://nodejs.org/dist/${NODEJS_VERSION}/node-${NODEJS_VERSION}-linux-x64.tar.xz > node-${NODEJS_VERSION}-linux-x64.tar.xz \
    && tar -Jxvf node-${NODEJS_VERSION}-linux-x64.tar.xz \
    && cd node-${NODEJS_VERSION}-linux-x64 \
    && mv bin/* /usr/local/bin \
    && mv include/* /usr/local/include/ \
    && mv lib/* /usr/local/lib \
    && cd /tmp \
    && rm -rf node-${NODEJS_VERSION}-linux-x64 node-${NODEJS_VERSION}-linux-x64.tar.xz 

RUN npm install -g npm@latest 
RUN npm version 
RUN npm install -g semver 

ADD bin/trigger_tag_deployment /usr/local/bin/trigger_tag_deployment
ADD bin/auto_version /usr/local/bin/auto_version
ADD bin/merge_request_version /usr/local/bin/merge_request_version
ADD bin/auto_tag /usr/local/bin/auto_tag
ADD lib/helpers /usr/local/lib/helpers
ADD test /usr/local/bin/test

ADD bin/bats /usr/local/bin/bats
ADD libexec/bats-core /usr/local/libexec/bats-core
ADD libexec/bats-assert /usr/local/libexec/bats-assert
ADD libexec/bats-support /usr/local/libexec/bats-support

RUN chmod +x /usr/local/bin/auto_version \
    && chmod +x /usr/local/bin/auto_tag \
    && chmod +x /usr/local/bin/test/*
