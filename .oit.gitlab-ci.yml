stages:
  - version_parity
  - build
  - test
  - publish
  - tag

include: '/auto-version.oit.yml'

variables:
  LOG_LEVEL: "info"
  VERSION_TAG: "v2.4.1"
  CANDIDATE_IMAGE_NAME: ${CI_PROJECT_NAME}:${CI_COMMIT_BRANCH}
  CANDIDATE_IMAGE_PATH: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}
  VERSION_IMAGE_PATH: ${CI_REGISTRY_IMAGE}:${VERSION_TAG}
  LATEST_IMAGE_PATH: ${CI_REGISTRY_IMAGE}:latest

check_version:
  stage: version_parity
  image: image-mirror-prod-registry.cloud.duke.edu/library/alpine
  script:
    - 'grep "Image Version: ${VERSION_TAG}" README.md'
    - grep ${VERSION_TAG} auto-version.yml
    - grep ${VERSION_TAG} Dockerfile
    - '[ "$(grep image auto-version.yml | sort -Vu | wc -l)" -eq 1 ]'
    - echo "Versions match ${VERSION_TAG}"
  only:
    refs:
      - branches
    changes:
      - auto-version.yml
      - README.md
      - Dockerfile

build:
  stage: build
  image: quay.io/buildah/stable
  script:
    - set -euxo pipefail
    - export STORAGE_DRIVER=vfs
    - export REGISTRY_AUTH_FILE=${HOME}/auth.json
    - echo "${CI_REGISTRY_PASSWORD}" | buildah login -u "${CI_REGISTRY_USER}" --password-stdin ${CI_REGISTRY}
    - buildah bud --isolation=chroot -f Dockerfile -t ${CANDIDATE_IMAGE_NAME}
    - CONTAINER_ID=$(buildah from ${CANDIDATE_IMAGE_NAME})
    - buildah commit ${CONTAINER_ID} ${CANDIDATE_IMAGE_PATH}
    - buildah push ${CANDIDATE_IMAGE_PATH}
  only:
    refs:
      - branches
  tags:
    - oit-shared-buildah

test:
  stage: test
  image: ${CANDIDATE_IMAGE_PATH}
  script:
    - /usr/local/bin/bats /usr/local/bin/test
  only:
    refs:
      - branches

publish:
  stage: publish
  image:
    name: quay.io/skopeo/stable
    entrypoint: [""]
  script:
    - THE_USER=${THE_USER:-gitlab-ci-token}
    - THE_TOKEN=${THE_TOKEN:-$CI_JOB_TOKEN}
    - echo "${THE_TOKEN}" | skopeo login -u ${THE_USER} --password-stdin ${CI_REGISTRY}
    - skopeo copy docker://${CANDIDATE_IMAGE_PATH} docker://${VERSION_IMAGE_PATH}
  only:
    - master

publish_latest:
  stage: publish
  image:
    name: quay.io/skopeo/stable
    entrypoint: [""]
  script:
    - THE_USER=${THE_USER:-gitlab-ci-token}
    - THE_TOKEN=${THE_TOKEN:-$CI_JOB_TOKEN}
    - echo "${THE_TOKEN}" | skopeo login -u ${THE_USER} --password-stdin ${CI_REGISTRY}
    - skopeo copy docker://${CANDIDATE_IMAGE_PATH} docker://${LATEST_IMAGE_PATH}
  only:
    - master

increment_version:
  stage: tag
  extends: .merge_request_version
  only:
    - master
